import re
import os
import requests
from bs4 import BeautifulSoup
from flask import Flask, render_template, redirect, url_for, request
#import scratch

site = "https://reddit.com"
folder_name = "downloads"
def scratch():
#site = input("Enter Site Name- ")


 response = requests.get(site)

 soup = BeautifulSoup(response.text, 'html.parser')
 img_tags = soup.find_all('img')

 urls = [img['src'] for img in img_tags]


 #folder_name = input("Enter Folder Name:- ")

 if not os.path.exists(folder_name):
  os.makedirs(folder_name)


 for url in urls:
    filename = re.search(r'/([\w_-]+[.](png))$', url)
    if not filename:
         print("Regex didn't match with the url: {}".format(url))
         continue

   # with open(filename.group(1), 'wb') as f:
    with open(os.path.join(folder_name, filename.group(1)), "wb") as f:
        if 'http' not in url:
            # sometimes an image source can be relative
            # if it is provide the base url which also happens
            # to be the site variable atm.
            url = '{}{}'.format(site, url)
        response = requests.get(url)

        f.write(response.content)


