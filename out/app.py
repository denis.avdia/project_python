from flask import Flask, render_template, redirect, url_for, request
import png
from png import *
app = Flask(__name__)


@app.route('/')
def home():
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':

        if request.form['username'] != 'admin' or request.form['password'] != 'admin':
            error = 'Invalid Credentials. Please try again with : admin/admin'
            # return redirect(url_for('welcome'))
            return render_template('login.html', error=error)

        else:
            png.scratch()
            directory = png.folder_name
            return {
                "Download status png_s": "PNG files are successfully to downloads folder",
                "URL": png.site,
                "Download Name Directory": png.folder_name,
                "ListFiles .png": os.listdir(directory)
             }

    return render_template('login.html')


#if __name__ == '__main__':
 #   app.run(debug=True)

