
# prepare yaml files for kubectl deployment from docker-composer.yaml

$path=((Get-Item .).FullName)
$file="$path\out\web-deployment.yaml"



if (-not(Test-Path -Path $file -PathType Leaf)) {

# download kompose.exe in order to convert docker-composer.yaml for deployment

        if (-not(Test-Path -Path kompose.exe -PathType Leaf)) {
Invoke-WebRequest -Uri https://github.com/kubernetes/kompose/releases/download/v1.26.1/kompose-windows-amd64.exe  -OutFile kompose.exe
          }
     

# convert the docker-composer for deployment
 
  cd out
 Start-process $path\kompose.exe convert

  cd ..

 Start-Sleep -s 2


# insert on the deployment file the "imagePullPolicy: Never" parameter 
     
  (Get-Content $file) | 
   Foreach-Object {
      $_ # send the current line to output
        if ($_ -match "image: web") 
        {
            #Add Lines after the selected pattern 
            "          imagePullPolicy: Never"
        }
    } | Set-Content $file


    }
   
  
     
minikube.exe start
Start-Sleep -s 2

minikube.exe docker-env

minikube docker-env | Invoke-Expression 
Start-Sleep -s 5
docker.exe build -t web $path\out\.

# build the images named "web"




